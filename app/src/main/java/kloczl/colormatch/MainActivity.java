package kloczl.colormatch;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    TextView scoreText, livesText, roundText, orderText, lostText;
    Button blueImage, greenImage, yellowImage, redImage, newGame;
    List<Integer> order;
    private int score = 0, round = 1, lives = 3, clickCount = 0, orderCount = 3;
    final Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        valuesOfVariables();
        updateTexts(score, round, lives);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                animateCards(order);
            }
        },750);
    }

    private void setListeners() {
        blueImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(order.get(clickCount) == 0){
                    score++;
                    clickCount++;
                    updateTexts(score, round, lives);
                    if(clickCount == orderCount){
                        score += orderCount;
                        orderCount++;
                        round++;
                        reOrder(orderCount);
                        removeListeners();
                        animateCards(order);
                    }
                }
                else{
                    score -= orderCount;
                    lives--;
                    if(lives == 0) {
                        lost();
                        return;
                    }
                    clickCount = 0;
                    round++;
                    updateTexts(score, round, lives);
                    reOrder(orderCount);
                    removeListeners();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            animateCards(order);
                        }
                    },750);
                }
            }
        });

        greenImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(order.get(clickCount) == 1){
                    score++;
                    clickCount++;
                    updateTexts(score, round, lives);
                    if(clickCount == orderCount){
                        score += orderCount;
                        orderCount++;
                        round++;
                        reOrder(orderCount);
                        removeListeners();
                        animateCards(order);
                    }
                }
                else{
                    score -= orderCount;
                    lives--;
                    if(lives == 0) {
                        lost();
                        return;
                    }
                    clickCount = 0;
                    round++;
                    updateTexts(score, round, lives);
                    reOrder(orderCount);
                    removeListeners();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            animateCards(order);
                        }
                    },750);
                }
            }
        });

        yellowImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(order.get(clickCount) == 2){
                    score++;
                    clickCount++;
                    updateTexts(score, round, lives);
                    if(clickCount == orderCount){
                        score += orderCount;
                        orderCount++;
                        round++;
                        reOrder(orderCount);
                        removeListeners();
                        animateCards(order);
                    }
                }
                else{
                    score -= orderCount;
                    lives--;
                    if(lives == 0) {
                        lost();
                        return;
                    }
                    clickCount = 0;
                    round++;
                    updateTexts(score, round, lives);
                    reOrder(orderCount);
                    removeListeners();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            animateCards(order);
                        }
                    },750);
                }
            }
        });

        redImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(order.get(clickCount) == 3){
                    score++;
                    clickCount++;
                    updateTexts(score, round, lives);
                    if(clickCount == orderCount){
                        score += orderCount;
                        orderCount++;
                        round++;
                        reOrder(orderCount);
                        removeListeners();
                        animateCards(order);
                    }
                }
                else{
                    score -= orderCount;
                    lives--;
                    if(lives == 0) {
                        lost();
                        return;
                    }
                    clickCount = 0;
                    round++;
                    updateTexts(score, round, lives);
                    reOrder(orderCount);
                    removeListeners();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            animateCards(order);
                        }
                    },750);
                }
            }
        });

        newGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                score = 0; round = 1; lives = 3; clickCount = 0; orderCount= 3;
                blueImage.setVisibility(View.VISIBLE);
                greenImage.setVisibility(View.VISIBLE);
                yellowImage.setVisibility(View.VISIBLE);
                redImage.setVisibility(View.VISIBLE);
                lostText.setVisibility(View.INVISIBLE);
                newGame.setVisibility(View.GONE);
                updateTexts(score, round, lives);
                reOrder(orderCount);
                removeListeners();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        animateCards(order);
                    }
                },1000);
            }
        });
    }

    private void valuesOfVariables() {
        scoreText = findViewById(R.id.score);
        livesText = findViewById(R.id.lives);
        roundText = findViewById(R.id.round);
        orderText = findViewById(R.id.order);
        lostText = findViewById(R.id.lost);
        blueImage = findViewById(R.id.blueCard);
        greenImage = findViewById(R.id.greenCard);
        yellowImage = findViewById(R.id.yellowCard);
        redImage = findViewById(R.id.redCard);
        newGame = findViewById(R.id.newGame);
        order = new ArrayList<>();

        lostText.setVisibility(View.INVISIBLE);
        newGame.setVisibility(View.GONE);
        reOrder(orderCount);
    }

    private void reOrder(int size){
        order.clear();
        int number;
        String numbers = "";
        for (int i = 0; i < size; i++){
            number = (int)(Math.random() * 4);
            order.add(number);
            numbers = numbers.concat(String.valueOf(number) + " ");
        }
        orderText.setText(numbers);
    }

    private void animateCards(final List<Integer> order){
        clickCount = 0;
        updateTexts(score, round, lives);
        Thread th = new Thread() {
            @Override
            public void run() {
                try {
                    for (int i = 0; i < order.size(); i++) {
                        final int k = i;
                        Thread.sleep(350);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    if(order.get(k) == 0){
                                        blueImage.setBackgroundColor(getResources().getColor(R.color.darkerBlueImageColor));
                                    }
                                    else if(order.get(k) == 1){
                                        greenImage.setBackgroundColor(getResources().getColor(R.color.darkerGreenImageId));
                                    }
                                    else if(order.get(k) == 2){
                                        yellowImage.setBackgroundColor(getResources().getColor(R.color.darkerYellowImageId));
                                    }
                                    else{
                                        redImage.setBackgroundColor(getResources().getColor(R.color.darkerRedImageId));
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                        Thread.sleep(750);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    if(order.get(k) == 0){
                                        blueImage.setBackgroundColor(getResources().getColor(R.color.blueImageColor));
                                    }
                                    else if(order.get(k) == 1){
                                        greenImage.setBackgroundColor(getResources().getColor(R.color.greenImageId));
                                    }
                                    else if(order.get(k) == 2){
                                        yellowImage.setBackgroundColor(getResources().getColor(R.color.yellowImageId));
                                    }
                                    else{
                                        redImage.setBackgroundColor(getResources().getColor(R.color.redImageId));
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                    setListeners();
                } catch (InterruptedException e) {
                }

            }
        };
        th.start();
    }

    private void updateTexts(int score, int round, int lives){
        scoreText.setText("Score: " + String.valueOf(score));
        roundText.setText("Round " + String.valueOf(round));
        livesText.setText("Lives: " + String.valueOf(lives));
    }

    private void removeListeners(){
        blueImage.setOnClickListener(null);
        greenImage.setOnClickListener(null);
        redImage.setOnClickListener(null);
        yellowImage.setOnClickListener(null);
    }
    
    private void lost(){
        blueImage.setVisibility(View.GONE);
        greenImage.setVisibility(View.GONE);
        yellowImage.setVisibility(View.GONE);
        redImage.setVisibility(View.GONE);
        lostText.setVisibility(View.VISIBLE);
        newGame.setVisibility(View.VISIBLE);
    }
}
